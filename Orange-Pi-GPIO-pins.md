Search terms: printout, wiring diagram.

Actual diagram:

People who have done it: https://www.youtube.com/watch?v=KF1IJvINjYE
https://www.youtube.com/watch?v=4zICevDOhr8

Mess with the GPIO:

https://www.youtube.com/watch?v=WCOHCHmrxaA

https://diyprojects.io/orange-pi-onelite-tutorial-use-gpio-python-pinouts/

Detailed tech breakdown and kernal info:

https://linux-sunxi.org/Orange_Pi_One

http://linux-sunxi.org/Fex_Guide

http://linux-sunxi.org/A20/PIO

Orientation:

https://forum.armbian.com/topic/1615-orange-pi-one-gpio-pin-layout/

How outside of process:

https://forum.armbian.com/topic/5818-powering-the-opi-zero-via-gpio/

GPIO power on Orange Pi One is definitely possible: http://linux-sunxi.org/images/7/7e/ORANGE_PI-ONE-V1_1.pdf

GPIO breakdown/best info:

"Ext Port": http://linux-sunxi.org/images/7/7e/ORANGE_PI-ONE-V1_1.pdf

http://www.orangepi.org/Docs/Pindefinition.html

USB GPIO on Orange Pi One is difficult, need someone with awesome solder skills: https://forum.armbian.com/topic/755-orange-pi-one-adding-usb-analog-audio-out-tv-out-mic-and-ir-receiver/
http://linux-sunxi.org/images/7/7e/ORANGE_PI-ONE-V1_1.pdf


Orange Pi GPIO - Interfacing

https://forum.armbian.com/topic/4940-how-to-turn-onoff-orangepi-pc-from-button-on-gpio/

  https://github.com/duxingkei33/orangepi_PC_gpio_pyH3

  https://gist.github.com/BiTinerary/2734ac46e80af2b1a2158c9f0ba79e25

https://diyprojects.io/orange-pi-onelite-tutorial-use-gpio-python-pinouts/

Adjust device default LEDs.


Raspi example of LED lighting:

https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins


Orange Pi USB process: https://diyprojects.io/orange-pi-onelite-tutorial-use-gpio-python-pinouts/

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install python-dev -y
sudo apt-get install python-pip -y
sudo apt-get install -y python-setuptools
pip install wheel
pip install pyA20

git clone https://github.com/duxingkei33/orangepi_PC_gpio_pyH3


This didn't work, BUT, the below did:

The general proceeding is described in https://linux-sunxi.org/GPIO

The GPIO pins are accessed via the sysfs file system. For enabling a specific pin it has to be exported into /sys/class/gpio/export

for pin PA1 the command would be echo 1 > sys/class/gpio/export. The pin number is calculated by the following formula :

 (position of letter in alphabet - 1) * 32 + pin number

PA1 has pin number ('A' is the 1st letter ) 1, PB2 has pin number 34 ('B' is 2nd letter ), PG7 has pin number 199( 'G' is 7th letter (7-1) * 32+7=199 )

the positions on the physical header are different again, cf. the graphic below

so for enabling PG7 that is pin 40 on the physical header can be used
(# echo "out" >/sys/class/gpio/gpio5/direction first)
  echo 199 > sys/class/gpio/export

Then you can run `echo "1" >/sys/class/gpio/gpio2/value` to turn it on and `echo "0" >/sys/class/gpio/gpio2/value` to turn it off.

You need to use a VCC3 pin and an IO pin for this. Refer to this diagram (Page 15): http://linux-sunxi.org/images/7/7e/ORANGE_PI-ONE-V1_1.pdf

I used pin 2 for VCC3 and pin 22 for IO.

https://stackoverflow.com/questions/46463724/accessing-gpio-on-orangepi-pc-plus-h3-on-armbian-3-4-113-and-newer

https://diyprojects.io/orange-pi-onelite-tutorial-use-gpio-python-pinouts/

https://forum.armbian.com/topic/1471-solved-difficulty-accessing-gpio-via-the-sunxi-gpio-export-interface/
