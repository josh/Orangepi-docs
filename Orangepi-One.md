# Initial Setup for the Orange Pi Zero

## Downloading and burning the image

Download Debian testing: https://www.armbian.com/orange-pi-one/

Download and install Etcher: https://etcher.io/

Run it to install the OS on the micro SD card.

I edited /etc/ssh/sshd_config on the SD card uncommenting this line:
`#Port 22`

#Logging in

Make sure you have a working Ethernet cable. (I didn't the first time.) Then login as root: root@IPADDRESS

The password as of this writing is 1234 and you MUST reset it upon logging in.

It will prompt you for your current password once and your new password two times.

## Other Setup

There is no need to resize the partition when using Armbian.

Run: `apt-get update && sudo apt-get upgrade` to update the software on the Orange Pi and

`apt-get install screen curl nano htop fail2ban rsync man git ca-certificates` to install some basic system utilities.

128MB Swap was automatically configured.

# Node

No issues installing node or NPM using the install script.

`npm install -g ursa` and `npm install -g sqlite` ran without a hitch. Much auto-compiling.

I used `npm list -g --depth=0` to confirm that the packages were successfully installed.

# Duck DNS
*I am following (and elaborating on) these instructions: https://www.duckdns.org/install.jsp*
*I setup the Orange Pi as ppl1.duckdns.org*

You can run ran `ps -ef | grep cr[o]n` to check and see if cron is installed, it is installed by default in Raspbian.

You can run `curl --version` to make sure curl is installed.

Then run `mkdir duckdns` to create the `duckdns` folder, then you can run: `cd duckdns` to enter the folder. Then you need to create `duck.sh`

Add this line into the file `echo url="https://www.duckdns.org/update?domains=example.duckdns.org&token=a7c4d0ad-114e-40ef-ba1d-d217904a50f2&ip=" | curl -k -o ~/duckdns/duck.log -K -`

(You need to replace `example.duckdns.org` with your Duck DNS URL for your domain and the gibberish after `token=` with the token you can find in your Duck DNS interface: https://www.duckdns.org/domains.)

I like more output showing that it was successful that just `OK` so I added `&verbose=true` right after `ip=` on that command above.

Then make the file executable using `sudo chmod 700`

Then we can setup cron to run the script every 5 minutes by running: `crontab -e` and adding this line to the bottom:

`*/5 * * * * ~/duckdns/duck.sh >/dev/null 2>&1`

Then save and exit.

Then test the script by running `./duck.sh`

You can then see if the attempt was successful by running: `cat duck.log`

# Weird but not important stuff

Had to run `sudo update-command-not-found` to update `command-not-found`

# Gitea setup

I setup Gitea using the following procedure:

Make a directory: `mkdir gitea`

Go into it: `cd gitea`

Download Gitea for ARMv7 boards: `curl -O https://dl.gitea.io/gitea/1.4.0/gitea-1.4.0-linux-arm-7`

Download the sha256sum: `curl -O https://dl.gitea.io/gitea/1.4.0/gitea-1.4.0-linux-arm-7.sha256`

Verify the binary: `sha256sum -c gitea-1.4.0-linux-arm-7.sha256`

It should come back with something like: `gitea-1.4.0-linux-arm-7: OK`

Assuming the verification works, mark the file as executable by running `sudo chmod 700 gitea-1.4.0-linux-arm-7`

Then run Gitea using: `./gitea-1.4.0-linux-arm-7 web`

Then go to `http://your-ip-address:3000/`

I chose to use SQLite. Specify the options applicable to your setup, make sure you give your domain name and application address.


# Go Setup

I followed these instructions: https://linode.com/docs/development/go/install-go-on-ubuntu/

I downloaded Go for armv6l.






# TODO

Make swap larger, install Gitea, etc.
