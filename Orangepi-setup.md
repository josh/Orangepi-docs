# Initial Setup for the Orange Pi Zero

## Downloading and burning the image

I downloaded Raspbian server for the Orange Pi Zero from MEGA Cloud here: http://www.orangepi.org/downloadresources/orangepizero/2017-05-05/orangepizero_e7c74a532b47c34968b5098.html

I followed the Ubuntu instructions here to burn the image to a Micro SD card: http://www.orangepi.org/Docs/SDcardinstallation.html

(Instructions copied here for archive purposes:)

Ubuntu(Linux)

    Insert your TF card into your computer. The size of TF should be larger than the OS image size, generally 4GB or greater.
    Format the TF card.
        Check the TF card node.

            `sudo fdisk -l`

        Delete all partition of TFcard.

            Use d command to delete all partition of TF card and use n command to add one new partition and use w command to save change.

            `sudo fdisk /dev/sdx`

        Format all the partition of TF card as FAT32.

            `sudo mkfs.vfat /dev/sdxx`

            (x should be replaced according to your TF card node)
            You can also jump this step under Linux, because write image command dd under Linux will format the TF card automatically.

    Download the OS image from the Downloads webpage.

        http://www.orangepi.org/downloadresources/

    Unzip the download file to get the OS image.

        `unzip [path]/[downloaded filename]``

        If the filename extension is .tgz, run the following command.

        `tar -zxvf [path]/[downloaded filename]``

        Ensure that neither the file name of the image you're using or the path contain any spaces (or other odd characters, for that matter).

    Write the image file to the TF card.
        Check the TF card node.

            `sudo fdisk -l`

        Verify if the hash key of the zip file is the same as shown on the downloads page (optional).

            `sha1sum [path]/[imagename]``

            This will print out a long hex number which should match the "SHA-1" line for the TF image you have downloaded.

        Unmount all the partition of the the TF card

            `umount /dev/sdxx`

        Write image file to TF card.

            `sudo dd bs=4M if=[path]/[imagename] of=/dev/sdx`

            Wait patiently to successfully complete writing. Please note that block size set to 4M will work most of the time, if not, please try 1M, although 1M will take considerably longer.You can use the command below to check progress.

            `sudo pkill -USR1 -n -x dd`

## Enable SSH

The Orange Pi doesn't have a display output.

I plugged my Micro SD card into my computer then created an empty file called `/boot/ssh`

Then I plugged my Micro SD card into the Orange PI and hooked up micro USB Power (5V/2A) and a working ethernet cable to the Orange Pi. I had previously tried using what I believe to be a bad ethernet cable.

I went into my router admin interface to find which IP address matched the Orange Pi that was plugged in via ethernet.

You can also use `arp` or `nmap` to discover this.

## SSH setup

Then I logged into the Orange PI using `ssh root@IPADDRESS` and entered the default username and password mentioned here: http://www.orangepi.org/downloadresources/orangepizero/2017-05-05/orangepizero_e7c74a532b47c34968b5098.html

Run `sudo passwd` as soon as you have access to your device to change the password. I found that non-root users were constantly running into `Killed` errors, so I ran through some SSH hardening (Setting up SSH key login) and left root logins enabled. First, add your SSH key to `~/.ssh/authorized_keys` as a new line, then save the file. You can disable password logins by opening `/etc/ssh/sshd_config` and changing `PermitRootLogin yes` to `PermitRootLogin without-password` and change the line that reads `PasswordAuthentication yes` to `PasswordAuthentication no` , you may have to uncomment it. Then save the file.

Then run: `sudo service ssh restart` to restart SSH.

## Resize the partition

After logging in, it will prompt you to resize the partition to make full use of the available space using: `fs_resize`

## Other Setup

Run: `apt-get update && sudo apt-get upgrade` to update the software on the Orange Pi and

`apt-get install screen curl nano htop fail2ban rsync man git ca-certificates` to install some basic system utilities.

I was met with a bunch of errors that said "sudo: unable to resolve host OrangePI" whenever I ran a command with sudo. To fix this. edit `/etc/hosts/` and add:

```
127.0.1.1    OrangePi
```

## Enable Swap

I followed this tutorial: https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04 to enable swap and set the swapiness to: `vm.swappiness=70`

## Curl complaining

If curl is complaining about out of date SSL certificates that are known to be good, run:

`apt-get install ca-certificates`

## Installing Golang
*See: https://linode.com/docs/development/go/install-go-on-ubuntu/*

## $GOPATH

Open `~/.profile` and add:

```
export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
```

to the bottom of the file.

## Install NodeJS

Run: `curl -fsSL bit.ly/node-installer | bash -s -- --no-dev-deps`

and follow the prompts.

# "Killed" errors

In non-root users, I have experienced a lot of "Killed" errors when running programs or tasks. I have experienced a few on the root user. Diagnostics: I looked at `htop` and saw that there was not high RAM usage, as I expected (https://stackoverflow.com/questions/726690/who-killed-my-process-and-why) I tried looking deeper the next day and found the same using `free -m` and `ps aux`. I was able to compile a basic "Hello World" Go script without any problems but it failed to compile Caddy, presumably because it is running Go 1.3.3 `npm install` is not working for `sqlite` or `ursa`, I was able to install `ursa` by running `git clone https://github.com/JoshKaufman/ursa.git` and `npm install`. I tried running `git clone https://github.com/kriasoft/node-sqlite.git` and `npm install` and it did not work. BASH scripts with CLI arguments aren't working properly either.

# Duck DNS
*I am following (and elaborating on) these instructions: https://www.duckdns.org/install.jsp*
*I setup the Orange Pi as ppl1.duckdns.org*

You can run ran `ps -ef | grep cr[o]n` to check and see if cron is installed, it is installed by default in Raspbian.

You can run `curl --version` to make sure curl is installed.

Then run `mkdir duckdns` to create the `duckdns` folder, then you can run: `cd duckdns` to enter the folder. Then you need to create `duck.sh`

Add this line into the file `echo url="https://www.duckdns.org/update?domains=example.duckdns.org&token=a7c4d0ad-114e-40ef-ba1d-d217904a50f2&ip=" | curl -k -o ~/duckdns/duck.log -K -`

(You need to replace `example.duckdns.org` with your Duck DNS URL for your domain and the gibberish after `token=` with the token you can find in your Duck DNS interface: https://www.duckdns.org/domains.)

I like more output showing that it was successful that just `OK` so I added `&verbose=true` right after `ip=` on that command above.

Then make the file executable using `sudo chmod 700`

Then we can setup cron to run the script every 5 minutes by running: `crontab -e` and adding this line to the bottom:

`*/5 * * * * ~/duckdns/duck.sh >/dev/null 2>&1`

Then save and exit.

Then test the script by running `./duck.sh`

You can then see if the attempt was successful by running: `cat duck.log`
